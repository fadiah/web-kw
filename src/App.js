import React from 'react';
import './App.css';
import {
Container,
Navbar,
Nav,
Carousel,
Button,
Image,
Row,
Col,
NavDropdown,
Form
} from 'react-bootstrap';

function Header(){
  return(
       
        <Navbar className="navbar" expand="md" fixed="top">
          <Container>
          <Navbar.Brand href="#home">
            <img
              alt=""
              src="https://www.jcodonuts.com/img/general/96d6f2e7e1f705ab5e59c84a6dc009b2.png"
              width="60"
              height="60"
              className="d-inline-block align-top"
            />
         </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse>
          <Nav className="link ml-auto">
            <Nav.Link href="#link" style={{color:"black"}}>DONUTS</Nav.Link>
            <Nav.Link href="#link" style={{color:"black"}}>COFFEE</Nav.Link>
            <Nav.Link href="#link" style={{color:"black"}}>MENU</Nav.Link>
            <Nav.Link href="#link" style={{color:"black"}}><img src="https://www.jcodonuts.com/img/general/2f0dc7807a5dbf2e6fc8e0b567921fbe.svg"/> STORES</Nav.Link>
            <Nav.Link href="#link" style={{color:"black"}}>CONTACT US</Nav.Link>
         </Nav>
        </Navbar.Collapse>
      </Container>
       </Navbar>
    );
}

function Banner(){
  return(
    <Container>
       <Carousel>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src="https://api.vold.dev.fleava.com/pictures/5b39cd517169294aba251f43/home/thumbnails/large_8dc69109-d620-4a92-b5a8-a4642209b626.jpg"
            alt="First slide"
            height="550px"
          />
          <Carousel.Caption className="carou-capt">
            <h3 className="htiga">SPRING BLOSSOMS</h3>
            <a href="#" className="a-carou"><b>Discovers The Flavors</b></a>
          </Carousel.Caption>
        </Carousel.Item>

        <Carousel.Item>
          <img
            className="d-block w-100"
            src="https://api.vold.dev.fleava.com/pictures/5b39cd517169294aba251f43/home_images_resized/thumbnails/large_8247205a-0141-42ca-a71b-f6302152e508.jpg"
            alt="Third slide"
             height="550px"
          />
          <Carousel.Caption className="carou-capt">
            <h3 className="htiga">DELICIOUS LIGHT MEALS</h3>
            <a href="#" className="a-carou"><b>LEARN MORE</b></a>
          </Carousel.Caption>
        </Carousel.Item>

        <Carousel.Item>
          <img
            className="d-block w-100"
            src="https://api.vold.dev.fleava.com/pictures/5b39cd517169294aba251f43/home/thumbnails/large_fed36549-c656-421c-9249-460179be3eff.jpg"
            alt="Third slide"
            height="550px"
          />
          <Carousel.Caption className="carou-capt">
            <h3 className="htiga">OUR CLASSICS</h3>
            <a href="#" className="a-carou"><b>SEE MORE</b></a>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>   
    </Container>
  );
}

function Main(){
  return(
   <Container className="main">
    <h3 className="isi-main">
      What’s your mood? We’ve got exceptionally handcrafted donuts, premium sourced Arabica coffee, and other crave-inducing treats prepared just for you.
    </h3>
    <Button variant="outline-warning">
      EXPLORE OUR MENU
    </Button>
   </Container>
  );
}

function Picture(){
  return(
    <div className="gambar">
      <Image src="https://api.vold.dev.fleava.com/pictures/5b39cd517169294aba251f43/home_images_resized/thumbnails/large_ab1f8255-ec8c-4ac1-8d8c-5d9c74638fbb.jpg" width="100%" height="550px" bsPrefix />
    </div>
  );
}

function Content(){
  const Plus = (props) =>{
    return(
      <Container className="isi-konten">
        <div className="letak">
          <div className="imej-konten">
            <Image src={props.image} fluid />
          </div>
            <br/><br/>
          <a href="#" className="link-konten">{props.link}</a>
          <a href="#"><hr/></a>
          <h6 className="desk-konten">{props.description}</h6>
        </div>
      </Container>
    );
  }
    return(
      <Row>
        <Col className="konten-pertama">
          <Plus
            image="https://api.vold.dev.fleava.com/pictures/5b39cd517169294aba251f43/images/thumbnails/large_68485e93-d997-4170-b562-9c8b87403a83.jpg"
            link="BE OUR PARTNER"
            description="Bring J.CO even closer to your community."
          />
        </Col>
        <Col className="konten-kedua">
          <Plus
            image="https://api.vold.dev.fleava.com/pictures/5b39cd517169294aba251f43/images/thumbnails/large_9e8ca736-9761-4dde-a571-4df707f1af5e.jpg"
            link="OUR LOCATION"
            description="Find a store near you."
            />
        </Col>
      </Row>
    );
}

function Footer(){
  return(
    <div className="footer">
      <Row>
        <Col> 
          <ul>
            <li className="ul-kesatu"><a href="#" className="a-kesatu">OUR STORY</a></li> 
            <li className="ul-kesatu"><a href="#" className="a-kesatu">OUR PRODUCTS</a></li> 
            <li className="ul-kesatu"><a href="#" className="a-kesatu">STORES</a></li>
            <li className="ul-kesatu"><a href="#" className="a-kesatu">ONLINE ORDER</a></li>
          </ul>
        </Col>
        <Col>
          <ul>
            <li className="ul-kesatu"><a href="#" className="a-kesatu">IN THE PRESS</a></li> 
            <li className="ul-kesatu"><a href="#" className="a-kesatu">EVENTS</a></li> 
            <li className="ul-kesatu"><a href="#" className="a-kesatu">FRANCHISE</a></li>
            <li className="ul-kesatu"><a href="#" className="a-kesatu">CONTACT US</a></li>
          </ul>
        </Col>
        <Col>
          <ul>
            <li className="ul-kesatu">
              <h8 className="li-hlapan">GET IN TOUCH</h8>
            </li> 
            <svg className="svg-pertama" width="9" height="17"><path d="M1.77889264 16.0573477V8.02766037H0V5.2608995h1.77889264V3.59982952C1.77889264 1.34284181 2.78281677 0 5.63687627 0h2.37548244v2.76777433H6.52768218c-1.11106013 0-1.1844782.3866371-1.1844782 1.10822454L5.33830945 5.2608995h2.69036439l-.31488195 2.76676087H5.33830945v8.02968733H1.77889264z" fill="#4A4A4A" fill-rule="evenodd"></path></svg>
            <svg className="svg-campur" width="15" height="14"><path d="M3.3337852.14695341c-1.4663309 0-2.6599501 1.19280996-2.6599501 2.65995002v8.44354077c0 1.4671401 1.1936192 2.6599501 2.6599501 2.6599501h8.4435408c1.4663308 0 2.65995-1.19281 2.65995-2.6599501V2.80690343c0-1.46714006-1.1936192-2.65995002-2.65995-2.65995002H3.3337852zm9.5109195 1.58528813V4.0652613l-2.3257366.0072831-.0080923-2.33301976 2.0295572-.00566463.3042717-.00161847zM7.5555556 4.60582918c.8084241 0 1.5237864.39976127 1.9640093 1.00992322.2872778.39814281.4588353.88530129.4588353 1.41292144 0 1.33523503-1.0868004 2.42203546-2.4228446 2.42203546-1.3352351 0-2.4220355-1.08680043-2.4220355-2.42203546 0-.52762015.1715575-1.01477863.4580261-1.41292144.440223-.61016195 1.1555852-1.00992322 1.9640094-1.00992322zm4.2217704 7.96366542H3.3337852c-.7275008 0-1.3190504-.5915496-1.3190504-1.3190504V5.6157524h2.0554527c-.1780313.43698601-.277567.91362446-.277567 1.41292144 0 2.07487436 1.6880607 3.76374436 3.7629351 3.76374436 2.0756835 0 3.7637443-1.68887 3.7637443-3.76374436 0-.49929698-.1011542-.97593543-.2775671-1.41292144h2.0546435v5.6346918c0 .7275008-.5915496 1.3190504-1.3190503 1.3190504z" fill="#4A4A4A" fill-rule="evenodd"></path></svg>
            <svg className="svg-campur" width="16" height="12"><path d="M15.1397849 8.9486368V3.10871082s0-2.81480401-2.5695182-2.81480401H2.7980322s-2.5686415 0-2.5686415 2.81480401V8.9486368s0 2.8148041 2.5686415 2.8148041h9.7722345s2.5695182 0 2.5695182-2.8148041m-4.7442163-3.27789274L5.6513522 8.8960573V2.44444444l4.7442164 3.22629962" fill="#4A4A4A" fill-rule="evenodd"></path></svg>
            <li className="ul-kesatu">
              <h8 className="li-hlapan">CHANGE REGION</h8>
               <NavDropdown title="INDONESIA">
                  <NavDropdown.Item href="#action/3.1">MALAYSIA</NavDropdown.Item>
                  <NavDropdown.Item href="#action/3.2">SOUTH KOREA</NavDropdown.Item>
                  <NavDropdown.Item href="#action/3.3">AMERIKA SERIKAT</NavDropdown.Item>
                </NavDropdown>
            </li> 
            <li className="ul-kesatu">
              <h8 className="li-hlapan">LANGUANGE</h8>
            </li>
            <a href="#" className="lang">EN</a>  |  <a href="#" className="lang">ID</a>
          </ul>
        </Col>
        <Col>
          <ul>
            <li className="ul-kesatu"><h8 href="#" className="li-hlapan">NEWSLETTER</h8></li>
              <Form>
                <Form.Group controlId="exampleForm.ControlInput1">
                <Col sm="10">
                 <Form.Control style={{backgroundColor:"#dee2e6"}} size="sm" type="text" placeholder="You@Example.com" />
                 <Button variant="outline-warning" size="sm">SIGN UP</Button>
                </Col>
                </Form.Group>
              </Form>
            <li className="ul-kesatu"><p>© 2021 J.CO Donuts. All Rights Reserved.</p></li>
            <li className="ul-kesatu"><a href="#" className="a-kesatu">Terms of Use</a>  | <a href="#" className="a-kesatu">Privacy Policy</a></li>
          </ul>
        </Col>
      </Row>
    </div>
  );
}

function App() {
  return (
    <div>
      <Header/>
      <Banner/>
      <Main/>
      <Picture/>
      <Content/>
      <Footer/>
    </div>
  );
}

export default App;
